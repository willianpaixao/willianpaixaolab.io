---
title: "Impossible List"
date: 2017-10-16T11:24:04+02:00
categories: ["lifestyle"]
keywords: ["list"]
draft: false
---
*Last update on August 21st 2019.*

## 5 latest achievements
  * ~~Bring my sister to Stockholm~~ [6/15/2018]
  * ~~Get top rope certified~~ [6/20/2018]
  * ~~Travel anywhere cool with my sister~~ [6/29/2018]
  * ~~Get lead climber certified~~ [10/11/2018]
  * ~~Ride through the Route1(Ring Road) in Iceland~~ [18/08/2019]

Totally based on [College Info Geek](https://collegeinfogeek.com/)'s [post](https://collegeinfogeek.com/about/meet-the-author/my-impossible-list/), I would like to build and maintain my own wish list of personal challenges.

## Life
  * Read 1000 books. [\[progress\]](https://www.goodreads.com/review/list/62493940?shelf=read)
  * Write and self-publish a book
  * Have enough money to retire (using the [4% rule](http://www.mrmoneymustache.com/2012/05/29/how-much-do-i-need-for-retirement/))

## Sports
  * BJJ
    * Earn a [Brazilian Jiu-Jitsu](https://en.wikipedia.org/wiki/Brazilian_jiu-jitsu) black belt
  * Bouldering/Climbing
    * ~~Get top rope climber certified~~ [6/20/2018]
    * ~~Get lead climber certified~~ [10/11/2018]
    * Climb a 7c route
  * Running
    * Run 5km in under 25 minutes
    * Run 10km in under 1 hour
    * Run a half marathon
    * Run a marathon
  * Triathlon
    * Finish a olympic triathlon
    * Finish a half [Ironman](http://ironman.com)
    * Finish an Ironman
  * Fitness
    * Do 100 push-ups in a single set
    * Do 100 pull-ups in a single set
    * Do 10 muscle-ups in a single set
  * Stretching
    * Do a perfect split

## Travel
### Travel list
  * ~~Bring my sister to Stockholm~~ [6/15/2018]
  * ~~Travel anywhere cool with my sister~~ [6/29/2018]
  * Bring my mother to Stockholm
  * See the northern lights
  * ~~Ride through the Route1(Ring Road) in Iceland~~ [18/08/2019]
  * Visit Machu Picchu
  * Visit Tokyo

### Living
  * Own an apartment in Brazil
  * Live in Germany for at least 1 year
  * Live in Italy for at least 1 year
  * Live as nomad for at least 1 year

## Professional
### Certifications
  * Renew my LPIC1
  * Pass the LPIC2
  * Get CKA certified
  * Renew my  Goethe-Zertifikat B1
  * Pass the Goethe-Zertifikat B2
  * ~~Complete German course on Duolingo.~~ [6/30/2017]
  * ~~Complete Italian course on Duolingo.~~ [7/25/2017]
  * Complete Swedish course on Duolingo

### Community
  * Actively contribute to an Open Source project
  * Own a repository with 1k stars in Github
  * ~~Earn a gold medal in Stack Overflow.~~ [4/11/2017]
  * Earn 10k points in a single Stack Overflow board
  * Publish an Android app on Google Store

### Computer languages
  * Learn Go and make something useful

## Weird
  * Live in a place without internet connection for at least 3 months
  * Practice Uberman Sleep Schedule for at least 30 straight days

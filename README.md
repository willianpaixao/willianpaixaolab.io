# willianpaixao.gitlab.io

[![pipeline status](https://gitlab.com/willianpaixao/willianpaixao.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/willianpaixao/willianpaixao.gitlab.io/commits/master)

For a while I have tried to build some kind of personal portfolio or blog.
This is my latest attempt.

### Previous attempts:

#### Wordpress blog
* [willianpaixao.wordpress.com](https://willianpaixao.wordpress.com)

Well, I have nothing against [WordPress](https://wordpress.org).
I think it's an amazing framework driven by an amazing company.
The free option is enough for a small personal blog like mine.
My only concern is that I would like to write markdown content then
build/deploy the website, using a pipeline and continuous delivery,
since that's basically my job.

#### Github pages
* [willianpaixao.github.io](https://willianpaixao.github.io)

[Github pages](https://pages.github.com) works great for simple repositories, using Jerkyll as framework.
That comes a little closer to what I wanted, but not quite. You can't use [Hugo](https://gohugo.io/) or another framework you want, and it's not flexible at all.
